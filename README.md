# Test28
## Для создания пользователя:
`php artisan auth:create-user`
## Для обновления списка марок и моделей:
1. Прописать Id и ключ от [BACK4APP](https://www.back4app.com/database/back4app/car-make-model-dataset) в .env
2. `php artisan app:refresh-vehicle-models `

