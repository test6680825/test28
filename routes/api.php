<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VehicleBrandController;
use App\Http\Controllers\VehicleModelController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::resource('brands', VehicleBrandController::class);
Route::resource('models', VehicleModelController::class);

Route::middleware('auth:sanctum')->resource('vehicles', VehicleController::class);

Route::controller(AuthController::class)->group(function () {
    Route::post('auth/login', 'login');
});
