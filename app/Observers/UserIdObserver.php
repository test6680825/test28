<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserIdObserver
{
    public function creating(Model $model)
    {
        if ($user = Auth::user()) {
            $model->user_id = $user->id;
        }
    }
}
