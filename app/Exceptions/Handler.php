<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->renderable(function (HttpExceptionInterface $exception, Request $request) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], $exception->getStatusCode());
        });
        $this->renderable(function (AuthenticationException $exception, Request $request) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], 401);
        });

        $this->renderable(function (ValidationException $exception, Request $request) {
            return response()->json([
                'message' => $exception->getMessage(),
            ], 400);
        });

        $this->renderable(function (Throwable $exception, Request $request) {
            if (App::environment('production')) {
                return response()->json([
                    'message' => 'Что-то пошло не так. Попробуйте позже'
                ], 500);
            }
            return response()->json([
                'type' => get_class($exception),
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace()
            ], 500);
        });
    }
}
