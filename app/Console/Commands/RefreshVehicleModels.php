<?php

namespace App\Console\Commands;

use App\Dto\Back4AppModelDTO;
use App\Models\VehicleBrand;
use App\Services\Back4AppService;
use Illuminate\Console\Command;

class RefreshVehicleModels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:refresh-vehicle-models';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление данных по маркам и моделям';

    /**
     * Execute the console command.
     */
    public function handle(Back4AppService $service)
    {
        $bar = $this->output->createProgressBar($service->count);
        foreach ($service->getData() as $chunk) {
            $results = collect($chunk);
            $results->each(function ($item, $key) use ($bar) {
                $back4AppDTO = new Back4AppModelDTO(...$item);
                $brand = VehicleBrand::firstOrCreate([
                    'name' => $back4AppDTO->Make,
                ]);
                $brand->models()->firstOrCreate([
                    'name' => $back4AppDTO->Model,
                ]);
                $bar->advance();
            });
        }
        $bar->finish();
    }
}
