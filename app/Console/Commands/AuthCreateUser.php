<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Validator;

class AuthCreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:create-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creating User';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $userFields = [
            'name' => $this->ask("Enter name:"),
            'email' => $this->ask("Enter email:"),
            'password' => $this->ask("Enter password:"),
            'c_password' => $this->ask("Enter c_password:"),
        ];
        $validator = Validator::make($userFields, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $this->error($validator->errors());
            return;
        }

        $userFields['password'] = bcrypt($userFields['password']);
        $user = User::create($userFields);
    }
}
