<?php

namespace App\Services;

use App\Models\Vehicle;
use App\Models\VehicleBrand;
use App\Models\VehicleModel;

class VehicleService
{
    public function create(array $data): Vehicle
    {
        return Vehicle::create($this->prepareData($data));
    }

    public function update(Vehicle $vehicle, array $data): Vehicle
    {
        $vehicle->update($this->prepareData($data));

        return $vehicle;
    }


    private function prepareData(array $data): array
    {
        if (($modelName = $data['model']) && $brandName = $data['brand']) {
            $brand = VehicleBrand::where(['name' => $brandName])->firstOrFail();
            $model = VehicleModel::where(['name' => $modelName, 'vehicle_brand_id' => $brand->id])->firstOrFail();
            $data['vehicle_model_id'] = $model->id;
        }

        return $data;
    }

}
