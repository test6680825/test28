<?php

namespace App\Services;

use App\Dto\Back4AppModelDTO;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\PendingRequest;
class Back4AppService
{
    const LIMIT = 3;

    public int $count;

    public function __construct()
    {
        $this->count = $this->getCount();
    }

    private function makeRequest(): PendingRequest
    {
        return Http::withHeaders([
            'X-Parse-Application-Id' => config('app.back4app_id'),
            'X-Parse-REST-API-Key' => config('app.back4app_key'),
        ]);
    }

    public function getData(): \Generator
    {
        $skip = 0;
        while ($skip < $this->count) {
            yield $this->makeRequest()->get(config('app.back4app_url'), [
                'count' => 1,
                'skip' => $skip,
                'limit' => self::LIMIT,
                'order' => 'Make',
                'keys' => 'Make,Model',
            ])->json()['results'];

            $skip += self::LIMIT;
        }
    }

    private function getCount(): int
    {
        return $this->makeRequest()->get(config('app.back4app_url'), [
            'count' => 1,
            'limit' => 0
        ])->json()['count'];
    }
}
