<?php

namespace App\Dto;

class Back4AppModelDTO
{
    public function __construct(
        public string $objectId,
        public string $Make,
        public string $Model,
        public string $createdAt,
        public string $updatedAt

    ) {}
}
