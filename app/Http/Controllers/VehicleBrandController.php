<?php

namespace App\Http\Controllers;

use App\Http\Resources\VehicleBrandCollection;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\VehicleBrandResource;
use App\Models\VehicleBrand;

class VehicleBrandController extends Controller
{
    public function index(): JsonResponse
    {
        return $this->sendResponse(new VehicleBrandCollection(VehicleBrand::paginate()), 'Brands collection');
    }

    /**
     * Display the specified resource.
     */
    public function show(VehicleBrand $vehicleBrand): JsonResponse
    {
        return $this->sendResponse(new VehicleBrandResource($vehicleBrand));
    }
}
