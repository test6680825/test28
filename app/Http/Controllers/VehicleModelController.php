<?php

namespace App\Http\Controllers;

use App\Http\Resources\VehicleModelCollection;
use App\Http\Resources\VehicleModelResource;
use App\Models\VehicleModel;
use Illuminate\Http\JsonResponse;

class VehicleModelController extends Controller
{
    public function index(): JsonResponse
    {
        return $this->sendResponse(new VehicleModelCollection(VehicleModel::paginate()), 'Models collection');
    }

    public function show(VehicleModel $vehicleModel): JsonResponse
    {
        return $this->sendResponse(new VehicleModelResource($vehicleModel));
    }
}
