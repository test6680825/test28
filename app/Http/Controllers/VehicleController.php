<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVehicleRequest;
use App\Http\Requests\UpdateVehicleRequest;
use App\Http\Resources\VehicleCollection;
use App\Http\Resources\VehicleResource;
use App\Models\Vehicle;
use App\Services\VehicleService;
use Illuminate\Http\JsonResponse;

class VehicleController extends Controller
{

    public function __construct(protected VehicleService $service)
    {
        $this->authorizeResource(Vehicle::class, 'vehicle');
    }

    public function index(): JsonResponse
    {
        return $this->sendResponse(new VehicleCollection(Vehicle::paginate()), 'Vehicles collection');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreVehicleRequest $request)
    {
        $vehicle = $this->service->create($request->validated());

        return $this->sendResponse(new VehicleResource($vehicle), 'Vehicle created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Vehicle $vehicle)
    {
        return $this->sendResponse(new VehicleResource($vehicle), 'Vehicle');
    }

    public function update(UpdateVehicleRequest $request, Vehicle $vehicle)
    {
        $this->service->update($vehicle, $request->validated());

        return $this->sendResponse(new VehicleResource($vehicle), 'Vehicle updated successfully.');
    }

    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();

        return $this->sendResponse([], 'Vehicle deleted successfully.');
    }
}
