<?php

namespace App\Http\Requests;

use App\Models\VehicleBrand;
use App\Models\VehicleModel;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateVehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'vehicle_model_id' => [
                Rule::requiredIf(!$this->model),
                Rule::exists(VehicleModel::class, 'id'),
            ],
            'model' => [
                'string',
                Rule::requiredIf(!$this->vehicle_model_id),
                Rule::exists(VehicleModel::class, 'name'),
            ],
            'brand' => [
                'string',
                Rule::requiredIf(!!$this->model),
                Rule::exists(VehicleBrand::class, 'name'),
            ],
            'year' => [
                'integer',
                'max:' . date('Y')
            ],
            'color' => 'string',
            'mileage' => 'integer',
        ];
    }
}
