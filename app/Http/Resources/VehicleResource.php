<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class VehicleResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'year' => $this->year,
            'mileage' => $this->mileage,
            'color' => $this->color,
            'brand' => new VehicleBrandResource($this->brand),
            'model' => new VehicleModelResource($this->model)
        ];
    }
}
