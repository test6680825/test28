<?php

namespace App\Models;

use App\Models\Scopes\UserScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $brand_id
 * @property int $model_id
 * @property int $user_id
 * @property int $year
 * @property int $mileage
 * @property string $color
 *
 * @property-read VehicleModel $model
 * @property-read VehicleBrand $brand
 */
class Vehicle extends Model
{
    use HasFactory;

    protected $fillable = [
        'vehicle_brand_id',
        'vehicle_model_id',
        'year',
        'mileage',
        'color',
        'user_id'
    ];

    public function model(): BelongsTo
    {
        return $this->belongsTo(VehicleModel::class, 'vehicle_model_id');
    }

    public function brand(): BelongsTo
    {
        return $this->model->brand();
    }

    protected static function booted()
    {
        static::addGlobalScope(new UserScope());
    }
}
